/****************************************************************************
|
|  \file   set.isat
|
|  \brief  Settings of isat and ffd defined by users
|
|  \Note   Please do not edit any comments
|
****************************************************************************/


/********************************************************************************
| Section 1: General settings of isat
********************************************************************************/
isat.useISAT 1 /*If use ISAT*/
isat.useBinarySelectedPoint 0 /*If use binary pre-training*/
isat.digAftdec 2 /*Digitals after decimal*/
isat.read_existing 0 /*1: read existing database; 0: train from scratch*/
isat.write_existing 0 /*1: write ffd results to database; 0: do not write*/
isat.err_global 0.3 /*user-defined global error tolerance*/
isat.num_input 4 /*number of isat inputs*/
isat.num_output 2 /*number of isat outputs*/


/********************************************************************************
| Section 2: Input settings of isat and ffd
********************************************************************************/
//-------------------------------------------------------------------------
// Section 2.0: Settings of numbers
// Note: Users define numbers of inlets, blocks and walls, respectively, if
//       there exist at least one inlet, block or wall that will be
//       overwirtten by isat inputs. The number should be 0 if none of inlet
//       block or wall that will be overwritten by isat inputs
//-------------------------------------------------------------------------
/*inpu.inpu_name: names of inputs including inlet_temp, inlet_mass, inlet_vel, block_temp, block_hea, rack_hea, sur_temp, sur_hea*/
inpu.inpu_name inlet_temp
inpu.inpu_name inlet_mass
inpu.inpu_name rack_hea
inpu.inpu_name sur_temp
/*number of inlets, blocks and walls*/
inpu.num_inlet 10
inpu.num_block 10
inpu.num_wall 6

//-------------------------------------------------------------------------
// Section 2.1: Settings of inlets
// Note: Users should define inlet_temp, inlet_u, inlet_v, inlet_w, 
//       respectively. The number of inlets should be consistent.
//-------------------------------------------------------------------------
/*inpu.dir_inlet: 1: x; 2: y; 3: z*/
inpu.dir_inlet 3

/*inpu.default_mass_flowrate: set a default mass flowrate if using prescribed outlet bc*/
inpu.default_mass_flowrate 2.83168

/*inlet_area_value: inlet area*/
inpu.inlet_area_value 3.71612
inpu.inlet_area_value 3.71612
inpu.inlet_area_value 3.71612
inpu.inlet_area_value 3.71612
inpu.inlet_area_value 3.71612
inpu.inlet_area_value 3.71612
inpu.inlet_area_value 3.71612
inpu.inlet_area_value 3.71612
inpu.inlet_area_value 3.71612
inpu.inlet_area_value 3.71612

/*inpu.inlet_temp_re: inlet_temp will be overwritten or not*/
inpu.inlet_temp_re 1
inpu.inlet_temp_re 1
inpu.inlet_temp_re 1
inpu.inlet_temp_re 1
inpu.inlet_temp_re 1
inpu.inlet_temp_re 1
inpu.inlet_temp_re 1
inpu.inlet_temp_re 1
inpu.inlet_temp_re 1
inpu.inlet_temp_re 1
/*inpu.inlet_vel_re: inlet_u will be overwritten or not*/
inpu.inlet_vel_re 1
inpu.inlet_vel_re 1
inpu.inlet_vel_re 1
inpu.inlet_vel_re 1
inpu.inlet_vel_re 1
inpu.inlet_vel_re 1
inpu.inlet_vel_re 1
inpu.inlet_vel_re 1
inpu.inlet_vel_re 1
inpu.inlet_vel_re 1

/*inpu.inlet_temp_wh: value of inlet_temp will be assigned by which isat input*/
inpu.inlet_temp_wh 1
inpu.inlet_temp_wh 1
inpu.inlet_temp_wh 1
inpu.inlet_temp_wh 1
inpu.inlet_temp_wh 1
inpu.inlet_temp_wh 1
inpu.inlet_temp_wh 1
inpu.inlet_temp_wh 1
inpu.inlet_temp_wh 1
inpu.inlet_temp_wh 1

/*inpu.inlet_vel_wh: value of inlet_u will be assigned by which isat input*/
inpu.inlet_vel_wh 2
inpu.inlet_vel_wh 2
inpu.inlet_vel_wh 2
inpu.inlet_vel_wh 2
inpu.inlet_vel_wh 2
inpu.inlet_vel_wh 2
inpu.inlet_vel_wh 2
inpu.inlet_vel_wh 2
inpu.inlet_vel_wh 2
inpu.inlet_vel_wh 2

//-------------------------------------------------------------------------
// Section 2.2: Settings of blocks
// Note: The number of blocks should be consistent
//-------------------------------------------------------------------------
/*inpu.block_re: temperature or heat flux of blocks will be overwritten or not*/
inpu.block_re 1
inpu.block_re 1
inpu.block_re 1
inpu.block_re 1
inpu.block_re 1
inpu.block_re 1
inpu.block_re 1
inpu.block_re 1
inpu.block_re 1
inpu.block_re 1

/*inpu.block_wh: temperature or heat flux of blocks will be assigned by which isat input*/
inpu.block_wh 3
inpu.block_wh 3
inpu.block_wh 3
inpu.block_wh 3
inpu.block_wh 3
inpu.block_wh 3
inpu.block_wh 3
inpu.block_wh 3
inpu.block_wh 3
inpu.block_wh 3

//-------------------------------------------------------------------------
// Section 2.3: Settings of walls
// Note: The number of walls should be consistent
//-------------------------------------------------------------------------
/*inpu.wall_re: temperature or heat flux of walls will be overwritten or not*/
inpu.wall_re 1
inpu.wall_re 1
inpu.wall_re 1
inpu.wall_re 1
inpu.wall_re 1
inpu.wall_re 1
/*inpu.wall_wh: temperature or heat flux of walls will be assigned by which isat input*/
inpu.wall_wh 4
inpu.wall_wh 4
inpu.wall_wh 4
inpu.wall_wh 4
inpu.wall_wh 4
inpu.wall_wh 4


/********************************************************************************
| Section 3: Output settings of isat and ffd
********************************************************************************/
/*outp.outp_name: names of outputs including temp_roo, temp_occ, vel_occ, temp_sen, vel_sen, temp_rack*/
outp.outp_name temp_roo
outp.outp_name temp_rack
/*outp.outp_weight: weights for error control, when outputs have different order of magnitudes*/
outp.outp_weight 1.0
outp.outp_weight 1.0